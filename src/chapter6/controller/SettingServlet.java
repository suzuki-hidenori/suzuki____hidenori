package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Branch;
import chapter6.beans.Department;
import chapter6.beans.User;
import chapter6.service.BranchService;
import chapter6.service.DepartmentService;
import chapter6.service.UserService;

@WebServlet(urlPatterns = {"/setting"})
public class SettingServlet extends HttpServlet{

		@Override
		protected void doGet(HttpServletRequest request,HttpServletResponse response)
				throws IOException,ServletException{

			User user = null;
			List<String> errorMessages = new ArrayList<String>();
			HttpSession session = ((HttpServletRequest) request).getSession();

			String urlNumber = request.getParameter("userId");
			if(urlNumber.matches("[0-9]{1,6}") && !(urlNumber.isEmpty())){
				int userId = Integer.valueOf(urlNumber);
				user = new UserService().select(userId);
			}
			if(user == null) {
				errorMessages.add("不正なパラメータが入力されました");
			}
			if(errorMessages.size() != 0) {
				session.setAttribute("errorMessages", errorMessages);
				request.setAttribute("user", user);
				response.sendRedirect("./manegement");
				return;
			}

			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();
			String branchName = request.getParameter("branchName");
			String departmentName  = request.getParameter("departmentName");

			request.setAttribute("branchName", branchName);
			request.setAttribute("departmentName", departmentName);
			request.setAttribute("user", user);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);

			request.getRequestDispatcher("setting.jsp").forward(request, response);
		}

		@Override
		protected void doPost(HttpServletRequest request,HttpServletResponse response)
		        throws IOException,ServletException{

			List<String> errorMessages = new ArrayList<String>();
			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();

			User user = getUser(request);
			if (!isValid(user, errorMessages)) {

				request.setAttribute("branches", branches);
				request.setAttribute("departments", departments);
				request.setAttribute("errorMessages", errorMessages);
				request.setAttribute("user", user);
				request.getRequestDispatcher("setting.jsp").forward(request, response);
				return;
			}

			new UserService().update(user);

			response.sendRedirect("./");
		}
		private User getUser(HttpServletRequest request) throws IOException, ServletException {
			User user = new User();
			user.setId(Integer.valueOf(request.getParameter("userId")));
			user.setAccount(request.getParameter("account"));
			user.setPassword(request.getParameter("password"));
			user.setCheckPassword(request.getParameter("checkPassword"));
			user.setName(request.getParameter("name"));
			user.setBranchId(Integer.valueOf(request.getParameter("branchId")));
			user.setDepartmentId(Integer.valueOf(request.getParameter("departmentId")));
			return user;
		}
		private boolean isValid(User user, List<String> errorMessages) {
			String account = user.getAccount();
			String password = user.getPassword();
			String checkPassword = user.getCheckPassword();
			String name = user.getName();
			String branchId = String.valueOf(user.getBranchId());
			String departmentId = String.valueOf(user.getDepartmentId());

			User duplicateAccount = new UserService().duplicateAccount(account);

			if(StringUtils.isEmpty(account)) {
				errorMessages.add("アカウント名を入力してください");
			}
			if(account.matches("[azAZ0*9]") || account.length() < 6) {
				errorMessages.add("アカウント名は６文字以上で入力してください");
			}else if(account.matches("[azAZ0*9]") || account.length() > 20) {
				errorMessages.add("アカウント名は２０文字以下で入力してください");
			}
			if(duplicateAccount != null && duplicateAccount.getId() != user.getId()) {
				errorMessages.add("アカウントが重複しています。");
			}
			if(StringUtils.isEmpty(password)) {
				errorMessages.add("パスワードを入力してください");
			} else if(!(password.equals(checkPassword))) {
				errorMessages.add("確認用とパスワードが違います");
			} else if(password.matches("[azAZ0*9=/:=@\\[-~]") || password.length() < 6 ) {
				errorMessages.add("パスワードは半角６文字以上で入力してください");
			} else if(password.matches("[azAZ0*9=/:=@\\[-~]") || password.length() > 20) {
				errorMessages.add("パスワードは半角２０文字以下で入力してください");
			}
			if(StringUtils.isEmpty(name)) {
				errorMessages.add("ユーザー名を入力してください");
			}
			if(!(StringUtils.isEmpty(name)) && (10 < name.length())) {
				errorMessages.add("ユーザー名は10文字以下で入力してください");
			}
			if(StringUtils.isEmpty(branchId)){
				errorMessages.add("支社を選んで下さい");
			}
			if(StringUtils.isEmpty(departmentId)){
				errorMessages.add("部署をを選んでください");
			}
			if(branchId.equals("1") && departmentId.equals("3") || branchId.equals("1") && departmentId.equals("4")) {
				errorMessages.add("支社と部署の組み合わせが違います。");
			} else if(branchId.equals("2") && departmentId.equals("1") || branchId.equals("2") && departmentId.equals("2")) {
				errorMessages.add("支社と部署の組み合わせが違います。");
			} else if((branchId.equals("3") && departmentId.equals("1") || branchId.equals("3") && departmentId.equals("2"))) {
				errorMessages.add("支社と部署の組み合わせが違います。");
			} else if(branchId.equals("4") && departmentId.equals("1") || branchId.equals("4") && departmentId.equals("2")) {
				errorMessages.add("支社と部署の組み合わせが違います。");
			}
			if(errorMessages.size() != 0) {
				return false;
			}
			return true;
		}

}
