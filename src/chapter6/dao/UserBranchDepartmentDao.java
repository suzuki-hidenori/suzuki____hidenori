package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserBranchDepartment;
import chapter6.exception.SQLRuntimeException;

public class UserBranchDepartmentDao {
		public List<UserBranchDepartment> select(Connection connection, int num) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            
	            sql.append("SELECT ");
	            sql.append("    users.id, ");
	            sql.append("    users.account, ");
	            sql.append("    users.name, ");
	            sql.append("    users.is_stopped, ");
	            sql.append("    users.created_date as created_date, ");
	            sql.append("    branches.id as branch_id, ");
	            sql.append("    branches.name as branch_name, ");
	            sql.append("    departments.id as department_id, ");
	            sql.append("    departments.name as department_name ");
	            sql.append("FROM departments ");
	            sql.append("INNER JOIN(branches INNER JOIN users ON branches.id = Users.branch_id)"); 
	            sql.append("ON departments.id = users.department_id");
	           
	            ps = connection.prepareStatement(sql.toString());
	            ResultSet rs = ps.executeQuery();
	            List<UserBranchDepartment> users = toUser(rs);
	            return users;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

	    private List<UserBranchDepartment> toUser(ResultSet rs) throws SQLException {

	        List<UserBranchDepartment> users = new ArrayList<UserBranchDepartment>();
	        try {
	            while (rs.next()) {
	            	UserBranchDepartment user = new UserBranchDepartment();
	            	user.setId(rs.getInt("id"));
	            	user.setAccount(rs.getString("account"));
	            	user.setName(rs.getString("name"));
	            	user.setIsStopped(rs.getInt("is_stopped"));
	            	user.setCreatedDate(rs.getTimestamp("created_date"));
	            	user.setBranchId(rs.getInt("branch_id"));
	            	user.setBranchName(rs.getString("branch_name"));
	            	user.setDepartmentId(rs.getInt("department_id"));
	            	user.setDepartmentName(rs.getString("department_name"));
	            	
	            	users.add(user);
	            }
	            return users;
	        } finally {
	            close(rs);
	        }
	    }
}
