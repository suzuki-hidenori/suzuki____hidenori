package chapter6.beans;

import java.util.Date;

public class Branch {
	private int id;
	private String branchName;
	
	private Date createdDate;
    private Date updatedDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return branchName;
	}
	public void setName(String branchName) {
		this.branchName = branchName;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpDate() {
		return updatedDate;
	}
	public void setUpDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
