package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Department;
import chapter6.exception.SQLRuntimeException;

public class DepartmentDao {
	public List<Department> select(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            
            sql.append("SELECT");
            sql.append("    departments.id, ");
            sql.append("    departments.name, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append("FROM departments ");
           
            ps = connection.prepareStatement(sql.toString());
            
            ResultSet rs = ps.executeQuery();
            List<Department> department = toUser(rs);
            return department;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	 private List<Department> toUser(ResultSet rs) throws SQLException {

	        List<Department> departments = new ArrayList<Department>();
	        try {
	            while (rs.next()) {
	            	Department department = new Department();
	            	department.setId(rs.getInt("departments.id"));
	            	department.setName(rs.getString("departments.name"));
	            	
	            	departments.add(department);
	            }
	            return departments;
	        } finally {
	            close(rs);
	        }
	 }
}
