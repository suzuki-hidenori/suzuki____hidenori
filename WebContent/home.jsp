<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>ホーム画面</title>
</head>
	<body>
		<div class="header">
			<a href="message">新規投稿</a>
				<c:if test="${loginUser.branchId == 1}">
        			<a href="manegement">ユーザー管理</a>
        		</c:if>
        	<a href="logout">ログアウト</a><br/>

        	<c:if test="${ not empty errorMessages }">
    	        <div class="errorMessages">
       				<ul>
           				<c:forEach items="${errorMessages}" var="errorMessage">
               				<li><c:out value="${errorMessage}" />
           				</c:forEach>
        			</ul>
    			</div>
   				<c:remove var="errorMessages" scope="session" />
		   	</c:if>

			<form  action="home" method="get">
				日付
				<input type="date" name="beginning" value="${beginning}"><br>
				~
				<input type="date" name="termination" value="${termination}"><br>
				カテゴリ
				<input type="text" name="searchWord" value="${searchWord}"><br>
				<input type="submit" value="絞り込み">
			</form>

			<div class="messages">
    			<c:forEach items="${messages}" var="message">
        			<div class="message">
            			<div class="account-name">
                			<span class="account"><c:out value="${message.account}" /></span>
                			<span class="title"><c:out value="${message.title}" /></span>
                			<span class="category"><c:out value="${message.category}" /></span>

                			<c:forEach var="s" items="${message.splitedText}">
            					<div class="text"><c:out value="${s}" /></div>
            				</c:forEach>

            				<div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
           	 			</div>
            		</div>
            		<c:if test="${loginUser.id == message.userId }">
            			<form action="deleteMessage" method="post" onsubmit="return check()" >
            				<input type="hidden" name="messageId" value=${message.id}  ><br />
            				<input type="submit" value="メッセージ削除"  >
            			</form>
            		</c:if>
            		<script>
						function check(){
						    var result = window.confirm('このコメントを削除します。よろしいですか？');

						    if( result == false ) {
						 		return false;
						    }
						}
					</script>
            		<div class="comment">
        				<c:forEach items="${comments}" var="comment">
        				    <c:set var="id" value="${message.id }"></c:set>
        				   	<c:if test="${id == comment.messageId }">
                				<span class="account"><c:out value="${comment.account}" /></span>

                				<c:forEach var="s" items="${comment.splitedText}">
            						<div class="text"><c:out value="${s}" /></div>
            					</c:forEach>

            					<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

            					<c:if test="${loginUser.id == comment.userId }">
            						<form action="deleteComment" method="post" onsubmit="return commentCheck()">
           			     				<input type="hidden" name="commentId" value=${comment.id} ><br />
           								<input type="submit" value="コメント削除" >
            						</form>
            					</c:if>
            				</c:if>
        			     </c:forEach>
        			</div>
        			<script>
						function commentCheck(){
						    var result = window.confirm('このコメントを削除します。よろしいですか？');

						    if( result == false ) {
						 		return false;
						    }
						}
					</script>
        				<form action="comments" method="post">
        			   		コメント<br />
        			   		<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
           					<input type="hidden" name="messageId" value="${message.id}" ><br />
           					<input type="submit" value="つぶやく">
       				</form>
    			 </c:forEach>
			 </div>
			 <div class="copyright">Copyright(c)suzuki_hidenori</div>
        </div>
	</body>
</html>