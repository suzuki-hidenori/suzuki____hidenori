package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException {
			
			int stopNumber = Integer.valueOf(request.getParameter("stopNumber"));
			int userId = Integer.valueOf(request.getParameter("userId"));
			new UserService().stop(stopNumber, userId);
	        response.sendRedirect("./manegement");
	}

}

