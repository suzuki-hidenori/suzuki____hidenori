package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter6.beans.User;
import chapter6.beans.UserBranchDepartment;
import chapter6.dao.UserBranchDepartmentDao;
import chapter6.dao.UserDao;
import chapter6.utils.CipherUtil;

public class UserService {

	public void insert(User user) {

		Connection connection = null;
		try {
			String encPassword = CipherUtil.encrypt(user.getPassword());

			user.setPassword(encPassword);

			connection = getConnection();
			new UserDao().insert(connection,user);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public User select(String account, String password) {

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(password);

            connection = getConnection();
            User user = new UserDao().select(connection, account, encPassword);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	public List<UserBranchDepartment> select() {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();
            List<UserBranchDepartment> users = new UserBranchDepartmentDao().select(connection, LIMIT_NUM);
            commit(connection);

            return users;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	public User select(int userId) {

        Connection connection = null;
        try {

            connection = getConnection();
            User user = new UserDao().select(connection, userId);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	public void update(User user) {
		Connection connection = null;
		try {
			String encPassword = CipherUtil.encrypt(user.getPassword());

			user.setPassword(encPassword);

			connection = getConnection();
			new UserDao().update(connection, user);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public void stop(int stopNumber, int userId) {
		Connection connection = null;
		try {
			connection = getConnection();
			new UserDao().stop(connection, stopNumber, userId);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public User duplicateAccount(String account) {

        Connection connection = null;
        try {

            connection = getConnection();
            User user = new UserDao().duplicateAccount(connection, account);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
