package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.Branch;
import chapter6.beans.Department;
import chapter6.beans.User;
import chapter6.beans.UserBranchDepartment;
import chapter6.service.BranchService;
import chapter6.service.DepartmentService;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/manegement" })
public class ManegementServlet extends HttpServlet{
	@Override
	   protected void doGet(HttpServletRequest request, HttpServletResponse response)
	         throws IOException, ServletException {

	        List<UserBranchDepartment> users = new UserService().select();
	        HttpSession session = request.getSession();
	        User loginUser = (User) session.getAttribute("loginUser");
	        List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();

			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments); 
	        request.setAttribute("loginUser", loginUser);
	        request.setAttribute("users", users);
	        request.getRequestDispatcher("/manegement.jsp").forward(request, response);
	   }
}
